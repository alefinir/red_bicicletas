var map = L.map('main_map').setView([-34.8833, -56.1667], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
//L.marker([-34.8833, -56.1667]).addTo(map);
//L.marker([-34.9, -56.178583333333]).addTo(map);

$.ajax({
    datatype: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})