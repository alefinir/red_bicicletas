var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');
//const { Router } = require('express');

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create );
router.post('/delete', bicicletaController.bicicleta_delete );
//test update
router.post('/update', bicicletaController.bicicleta_update );

module.exports = router;
