var Bicicleta = require ('../../models/bicicleta');

const { allBicis } = require('../../models/bicicleta');
const { render } = require('../../app');

/*
exports.bicicleta_list = function(req, res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}
*/
exports.bicicleta_list = function(req, res) {
    Bicicleta.find({}, function(err, bicicletas){
           res.status(200).json({
               bicicletas : bicicletas
            });
    });
}

/*
exports.bicicleta_create = function(req, res){
    //var bici = new Bicicleta(req.body.code, req.body.color, req.body.modelo);

    var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo});
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
}*/


exports.bicicleta_create = function(req, res) {

    var bicicleta = new Bicicleta({
        code:req.body.id,
        color:req.body.color,
        modelo:req.body.modelo
    });
    console.log("req es -->", req.body);
    bicicleta.ubicacion = [req.body.lat, req.body.lng];
    
    //bicicleta.ubicacion = [44, 44];

    bicicleta.save(function( err) {
        if( err ) console.log(err);
        res.status(200).json(bicicleta);
    });

};

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}

//test update
exports.bicicleta_update = function(req, res){
    var bici = Bicicleta.findById(req.body.id);

    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.status(200).json({
        bicicleta: bici
    });
}