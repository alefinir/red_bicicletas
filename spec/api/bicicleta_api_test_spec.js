var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');
var request = require('request');

var base_url = "http://localhost:3000:/api/bicicletas";

describe('Bicicleta API', () => {

    //add mongo

        //test
    //beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach(function(done) {
        //test again
        //mongoose.disconnect();
    
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true});
    
        /*set timer */
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database');
            done();
        });
    });
    
        afterEach(function(done) {
            Bicicleta.deleteMany({}, function(err, success){
                if (err) console.log(err);
                done();
            });
        });

        //fin add mongo


    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, 'negro', 'urbana', [-34.8833, -56.1667]);

            Bicicleta.add(a);
            request.get('http://localhost:3000/api/bicicletas', function(error, response,body){
                expect(response.statusCode).toBe(200);
            });


        });
    });



    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';

            request.post({
                headers: headers,
                url:    'http://localhost:3000/api/bicicletas/create',
                body:   aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo");
                done();
            });
        });
    });

    //falta test delete bicicletas
});